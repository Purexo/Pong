var Random = Sup.Math.Random;

var scoreP1  : number = 0;
var scoreP2  : number = 0;
var scoreTmp : number = 0;

var MAXSCORE : number = 500;

var trajectoireBalle : Sup.Math.Vector3;

var gravite : Sup.Math.Vector3;

var CONTROLE_P1 : {[key:string] : any} = {
  UP       : ["W", "Z"],
  DOWN     : ["S"],
  LEFT     : ["A", "Q"],
  RIGHT    : ["D"]
};

var CONTROLE_P2 : {[key:string] : any} = {
  UP       : ["UP"],
  DOWN     : ["DOWN"],
  LEFT     : ["LEFT"],
  RIGHT    : ["RIGHT"]
};

var CONTROLE_BALL : {[key:string] : any} = {
  RELOAD   : ["X", "R"],
  ECHAP    : ["ESCAPE"]
}