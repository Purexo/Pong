(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var CameraMarker_1 = require("./CameraMarker");
SupEngine.registerEditorComponentClass("CameraMarker", CameraMarker_1.default);

},{"./CameraMarker":2}],2:[function(require,module,exports){
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var THREE = SupEngine.THREE;
var CameraUpdater_1 = require("./CameraUpdater");
var CameraMarker = (function (_super) {
    __extends(CameraMarker, _super);
    function CameraMarker(actor, config) {
        _super.call(this, actor, "Marker");
        this.viewport = { x: 0, y: 0, width: 1, height: 1 };
        this.projectionNeedsUpdate = true;
        if (config != null)
            this.setConfig(config);
        var geometry = new THREE.Geometry();
        for (var i = 0; i < 24; i++)
            geometry.vertices.push(new THREE.Vector3(0, 0, 0));
        this.line = new THREE.Line(geometry, new THREE.LineBasicMaterial({ color: 0xffffff, opacity: 0.5, transparent: true }), THREE.LinePieces);
        this.actor.threeObject.add(this.line);
        this.line.updateMatrixWorld(false);
    }
    CameraMarker.prototype.setConfig = function (config) {
        this.setOrthographicMode(config.mode === "orthographic");
        this.setFOV(config.fov);
        this.setOrthographicScale(config.orthographicScale);
        this.setViewport(config.viewport.x, config.viewport.y, config.viewport.width, config.viewport.height);
    };
    CameraMarker.prototype.setOrthographicMode = function (isOrthographic) {
        this.isOrthographic = isOrthographic;
        this.projectionNeedsUpdate = true;
    };
    CameraMarker.prototype.setFOV = function (fov) {
        this.fov = fov;
        if (!this.isOrthographic)
            this.projectionNeedsUpdate = true;
    };
    CameraMarker.prototype.setOrthographicScale = function (orthographicScale) {
        this.orthographicScale = orthographicScale;
        if (this.isOrthographic)
            this.projectionNeedsUpdate = true;
    };
    CameraMarker.prototype.setViewport = function (x, y, width, height) {
        this.viewport.x = x;
        this.viewport.y = y;
        this.viewport.width = width;
        this.viewport.height = height;
        this.projectionNeedsUpdate = true;
    };
    CameraMarker.prototype.setRatio = function (ratio) {
        this.ratio = ratio;
        this.projectionNeedsUpdate = true;
    };
    CameraMarker.prototype._resetGeometry = function () {
        var near = 0.1;
        var far = 500;
        var farTopRight;
        var nearTopRight;
        if (this.isOrthographic) {
            var right = this.orthographicScale / 2;
            if (this.ratio != null)
                right *= this.ratio;
            farTopRight = new THREE.Vector3(right, this.orthographicScale / 2, far);
            nearTopRight = new THREE.Vector3(right, this.orthographicScale / 2, near);
        }
        else {
            var tan = Math.tan(THREE.Math.degToRad(this.fov / 2));
            farTopRight = new THREE.Vector3(far * tan, far * tan, far);
            nearTopRight = farTopRight.clone().normalize().multiplyScalar(0.1);
        }
        // Near plane
        this.line.geometry.vertices[0].set(-nearTopRight.x, nearTopRight.y, -near);
        this.line.geometry.vertices[1].set(nearTopRight.x, nearTopRight.y, -near);
        this.line.geometry.vertices[2].set(nearTopRight.x, nearTopRight.y, -near);
        this.line.geometry.vertices[3].set(nearTopRight.x, -nearTopRight.y, -near);
        this.line.geometry.vertices[4].set(nearTopRight.x, -nearTopRight.y, -near);
        this.line.geometry.vertices[5].set(-nearTopRight.x, -nearTopRight.y, -near);
        this.line.geometry.vertices[6].set(-nearTopRight.x, -nearTopRight.y, -near);
        this.line.geometry.vertices[7].set(-nearTopRight.x, nearTopRight.y, -near);
        // Far plane
        this.line.geometry.vertices[8].set(-farTopRight.x, farTopRight.y, -far);
        this.line.geometry.vertices[9].set(farTopRight.x, farTopRight.y, -far);
        this.line.geometry.vertices[10].set(farTopRight.x, farTopRight.y, -far);
        this.line.geometry.vertices[11].set(farTopRight.x, -farTopRight.y, -far);
        this.line.geometry.vertices[12].set(farTopRight.x, -farTopRight.y, -far);
        this.line.geometry.vertices[13].set(-farTopRight.x, -farTopRight.y, -far);
        this.line.geometry.vertices[14].set(-farTopRight.x, -farTopRight.y, -far);
        this.line.geometry.vertices[15].set(-farTopRight.x, farTopRight.y, -far);
        // Lines
        this.line.geometry.vertices[16].set(-nearTopRight.x, nearTopRight.y, -near);
        this.line.geometry.vertices[17].set(-farTopRight.x, farTopRight.y, -far);
        this.line.geometry.vertices[18].set(nearTopRight.x, nearTopRight.y, -near);
        this.line.geometry.vertices[19].set(farTopRight.x, farTopRight.y, -far);
        this.line.geometry.vertices[20].set(nearTopRight.x, -nearTopRight.y, -near);
        this.line.geometry.vertices[21].set(farTopRight.x, -farTopRight.y, -far);
        this.line.geometry.vertices[22].set(-nearTopRight.x, -nearTopRight.y, -near);
        this.line.geometry.vertices[23].set(-farTopRight.x, -farTopRight.y, -far);
        this.line.geometry.verticesNeedUpdate = true;
    };
    CameraMarker.prototype._destroy = function () {
        this.actor.threeObject.remove(this.line);
        this.line.geometry.dispose();
        this.line.material.dispose();
        this.line = null;
        _super.prototype._destroy.call(this);
    };
    CameraMarker.prototype.update = function () {
        if (this.projectionNeedsUpdate) {
            this.projectionNeedsUpdate = false;
            this._resetGeometry();
        }
    };
    CameraMarker.Updater = CameraUpdater_1.default;
    return CameraMarker;
})(SupEngine.ActorComponent);
exports.default = CameraMarker;

},{"./CameraUpdater":3}],3:[function(require,module,exports){
var CameraUpdater = (function () {
    function CameraUpdater(projectClient, camera, config) {
        var _this = this;
        this.onResourceReceived = function (resourceId, resource) {
            _this.resource = resource;
            _this.updateRatio();
        };
        this.onResourceEdited = function (resourceId, command, propertyName) {
            _this.updateRatio();
        };
        this.projectClient = projectClient;
        this.camera = camera;
        this.config = config;
        this.camera.setConfig(this.config);
        this.camera.setRatio(5 / 3);
        this.projectClient.subResource("gameSettings", this);
    }
    CameraUpdater.prototype.destroy = function () {
        if (this.resource != null)
            this.projectClient.unsubResource("gameSettings", this);
    };
    CameraUpdater.prototype.config_setProperty = function (path, value) {
        this.camera.setConfig(this.config);
    };
    CameraUpdater.prototype.updateRatio = function () {
        if (this.resource.pub.ratioNumerator != null && this.resource.pub.ratioDenominator != null)
            this.camera.setRatio(this.resource.pub.ratioNumerator / this.resource.pub.ratioDenominator);
        else
            this.camera.setRatio(5 / 3);
    };
    return CameraUpdater;
})();
exports.default = CameraUpdater;

},{}]},{},[1]);
