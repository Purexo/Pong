(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var CannonBody_1 = require("./CannonBody");
var CannonBodyMarker_1 = require("./CannonBodyMarker");
SupEngine.registerComponentClass("CannonBody", CannonBody_1.default);
SupEngine.registerEditorComponentClass("CannonBodyMarker", CannonBodyMarker_1.default);

},{"./CannonBody":2,"./CannonBodyMarker":3}],2:[function(require,module,exports){
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var CannonBody = (function (_super) {
    __extends(CannonBody, _super);
    function CannonBody(actor, config) {
        _super.call(this, actor, "CannonBody");
        if (config == null)
            config = {};
        this.body = new window.CANNON.Body();
        SupEngine.Cannon.World.addBody(this.body);
        if (config.shape != null)
            this.setup(config);
    }
    CannonBody.prototype.setup = function (config) {
        this.mass = config.mass != null ? config.mass : 0;
        this.fixedRotation = config.fixedRotation != null ? config.fixedRotation : false;
        this.offsetX = config.offsetX != null ? config.offsetX : 0;
        this.offsetY = config.offsetY != null ? config.offsetY : 0;
        this.offsetZ = config.offsetZ != null ? config.offsetZ : 0;
        this.actorPosition = this.actor.getGlobalPosition();
        this.actorOrientation = this.actor.getGlobalOrientation();
        this.body.mass = this.mass;
        this.body.type = this.mass === 0 ? window.CANNON.Body.STATIC : window.CANNON.Body.DYNAMIC;
        this.body.material = SupEngine.Cannon.World.defaultMaterial;
        this.body.fixedRotation = this.fixedRotation;
        this.body.updateMassProperties();
        this.shape = config.shape;
        switch (this.shape) {
            case "box":
                this.halfWidth = config.halfWidth != null ? config.halfWidth : 0.5;
                this.halfHeight = config.halfHeight != null ? config.halfHeight : 0.5;
                this.halfDepth = config.halfDepth != null ? config.halfDepth : 0.5;
                this.body.addShape(new window.CANNON.Box(new window.CANNON.Vec3(this.halfWidth, this.halfHeight, this.halfDepth)));
                break;
            case "sphere":
                this.radius = config.radius != null ? config.radius : 1;
                this.body.addShape(new window.CANNON.Sphere(this.radius));
                break;
            case "cylinder":
                this.radius = config.radius != null ? config.radius : 1;
                this.height = config.height != null ? config.height : 1;
                this.body.addShape(new window.CANNON.Cylinder(this.radius, this.radius, this.height, 20));
                break;
        }
        this.body.position.set(this.actorPosition.x, this.actorPosition.y, this.actorPosition.z);
        this.body.shapeOffsets[0].set(this.offsetX, this.offsetY, this.offsetZ);
        this.body.quaternion.set(this.actorOrientation.x, this.actorOrientation.y, this.actorOrientation.z, this.actorOrientation.w);
    };
    CannonBody.prototype.update = function () {
        this.actorPosition.set(this.body.position.x, this.body.position.y, this.body.position.z);
        this.actor.setGlobalPosition(this.actorPosition);
        this.actorOrientation.set(this.body.quaternion.x, this.body.quaternion.y, this.body.quaternion.z, this.body.quaternion.w);
        this.actor.setGlobalOrientation(this.actorOrientation);
    };
    CannonBody.prototype._destroy = function () {
        SupEngine.Cannon.World.remove(this.body);
        this.body = null;
        _super.prototype._destroy.call(this);
    };
    return CannonBody;
})(SupEngine.ActorComponent);
exports.default = CannonBody;

},{}],3:[function(require,module,exports){
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var THREE = SupEngine.THREE;
var CannonBodyMarkerUpdater_1 = require("./CannonBodyMarkerUpdater");
var CannonBodyMarker = (function (_super) {
    __extends(CannonBodyMarker, _super);
    function CannonBodyMarker(actor) {
        _super.call(this, actor, "CannonBodyMarker");
    }
    CannonBodyMarker.prototype.setBox = function (size) {
        if (this.mesh != null)
            this._clearRenderer();
        var geometry = new THREE.BoxGeometry(size.halfWidth * 2, size.halfHeight * 2, size.halfDepth * 2);
        var material = new THREE.MeshBasicMaterial({ wireframe: true, color: 0xf459e4, transparent: true, opacity: 0.2 });
        this.mesh = new THREE.Mesh(geometry, material);
        this.actor.threeObject.add(this.mesh);
        this.mesh.updateMatrixWorld(false);
    };
    CannonBodyMarker.prototype.setSphere = function (radius) {
        if (this.mesh != null)
            this._clearRenderer();
        var geometry = new THREE.SphereGeometry(radius);
        var material = new THREE.MeshBasicMaterial({ wireframe: true, color: 0xf459e4, transparent: true, opacity: 0.2 });
        this.mesh = new THREE.Mesh(geometry, material);
        this.actor.threeObject.add(this.mesh);
        this.mesh.updateMatrixWorld(false);
    };
    CannonBodyMarker.prototype.setCylinder = function (radius, height) {
        if (this.mesh != null)
            this._clearRenderer();
        var geometry = new THREE.CylinderGeometry(radius, radius, height);
        var material = new THREE.MeshBasicMaterial({ wireframe: true, color: 0xf459e4, transparent: true, opacity: 0.2 });
        this.mesh = new THREE.Mesh(geometry, material);
        this.mesh.quaternion.setFromAxisAngle(new THREE.Vector3(1, 0, 0), Math.PI / 2);
        this.actor.threeObject.add(this.mesh);
        this.mesh.updateMatrixWorld(false);
    };
    CannonBodyMarker.prototype.setOffset = function (offset) {
        this.mesh.position.setX(offset.x);
        this.mesh.position.setY(offset.y);
        this.mesh.position.setZ(offset.z);
        this.mesh.updateMatrixWorld(false);
    };
    CannonBodyMarker.prototype._clearRenderer = function () {
        this.actor.threeObject.remove(this.mesh);
        this.mesh.traverse(function (obj) {
            if (obj.dispose != null)
                obj.dispose();
        });
        this.mesh = null;
    };
    CannonBodyMarker.prototype._destroy = function () {
        if (this.mesh != null)
            this._clearRenderer();
        _super.prototype._destroy.call(this);
    };
    CannonBodyMarker.Updater = CannonBodyMarkerUpdater_1.default;
    return CannonBodyMarker;
})(SupEngine.ActorComponent);
exports.default = CannonBodyMarker;

},{"./CannonBodyMarkerUpdater":4}],4:[function(require,module,exports){
var CannonBodyMarkerUpdater = (function () {
    function CannonBodyMarkerUpdater(client, bodyRenderer, config) {
        this.client = client;
        this.bodyRenderer = bodyRenderer;
        this.config = config;
        switch (this.config.shape) {
            case "box":
                this.bodyRenderer.setBox({
                    halfWidth: this.config.halfWidth,
                    halfHeight: this.config.halfHeight,
                    halfDepth: this.config.halfDepth
                });
                break;
            case "sphere":
                this.bodyRenderer.setSphere(this.config.radius);
                break;
            case "cylinder":
                this.bodyRenderer.setCylinder(this.config.radius, this.config.height);
                break;
        }
        this.bodyRenderer.setOffset({ x: this.config.offsetX, y: this.config.offsetY, z: this.config.offsetZ });
    }
    CannonBodyMarkerUpdater.prototype.config_setProperty = function (path, value) {
        this.config[path] = value;
        if (["halfWidth", "halfHeight", "halfDepth"].indexOf(path) !== -1 || (path === "shape" && value === "box")) {
            this.bodyRenderer.setBox({
                halfWidth: this.config.halfWidth,
                halfHeight: this.config.halfHeight,
                halfDepth: this.config.halfDepth
            });
            this.bodyRenderer.setOffset({ x: this.config.offsetX, y: this.config.offsetY, z: this.config.offsetZ });
        }
        if (["offsetX", "offsetY", "offsetZ"].indexOf(path) !== -1)
            this.bodyRenderer.setOffset({ x: this.config.offsetX, y: this.config.offsetY, z: this.config.offsetZ });
        if ((path === "radius" && this.config.shape === "cylinder") || (path === "shape" && value === "cylinder") || path === "height") {
            this.bodyRenderer.setCylinder(this.config.radius, this.config.height);
            this.bodyRenderer.setOffset({ x: this.config.offsetX, y: this.config.offsetY, z: this.config.offsetZ });
        }
        if ((path === "radius" && this.config.shape === "sphere") || (path === "shape" && value === "sphere")) {
            this.bodyRenderer.setSphere(this.config.radius);
            this.bodyRenderer.setOffset({ x: this.config.offsetX, y: this.config.offsetY, z: this.config.offsetZ });
        }
    };
    return CannonBodyMarkerUpdater;
})();
exports.default = CannonBodyMarkerUpdater;

},{}]},{},[1]);
