(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var SpriteRenderer_1 = require("./SpriteRenderer");
SupEngine.registerComponentClass("SpriteRenderer", SpriteRenderer_1.default);

},{"./SpriteRenderer":2}],2:[function(require,module,exports){
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var THREE = SupEngine.THREE;
var SpriteRendererUpdater_1 = require("./SpriteRendererUpdater");
var SpriteRenderer = (function (_super) {
    __extends(SpriteRenderer, _super);
    function SpriteRenderer(actor, spriteAsset) {
        _super.call(this, actor, "SpriteRenderer");
        this.opacity = 1;
        this.color = { r: 1, g: 1, b: 1 };
        this.hasFrameBeenUpdated = false;
        if (spriteAsset != null)
            this.setSprite(spriteAsset);
    }
    SpriteRenderer.prototype.setSprite = function (asset) {
        this._clearMesh();
        this.asset = asset;
        this.animationName = null;
        this.animationsByName = {};
        if (this.asset == null)
            return;
        this.updateAnimationsByName();
        this.geometry = new THREE.PlaneBufferGeometry(this.asset.grid.width, this.asset.grid.height);
        this.material = new THREE.MeshBasicMaterial({
            map: this.asset.texture,
            alphaTest: this.asset.alphaTest,
            side: THREE.DoubleSide,
            transparent: true,
            opacity: this.opacity
        });
        this.material.color.setRGB(this.color.r, this.color.g, this.color.b);
        this.threeMesh = new THREE.Mesh(this.geometry, this.material);
        var scaleRatio = 1 / this.asset.pixelsPerUnit;
        this.threeMesh.scale.set(scaleRatio, scaleRatio, scaleRatio);
        this.threeMesh.position.setX((0.5 - this.asset.origin.x) * this.asset.grid.width * scaleRatio);
        this.threeMesh.position.setY((0.5 - this.asset.origin.y) * this.asset.grid.height * scaleRatio);
        this.setFrame(0);
        this.actor.threeObject.add(this.threeMesh);
        this.threeMesh.updateMatrixWorld(false);
    };
    SpriteRenderer.prototype.updateAnimationsByName = function () {
        this.animationsByName = {};
        for (var _i = 0, _a = this.asset.animations; _i < _a.length; _i++) {
            var animation = _a[_i];
            this.animationsByName[animation.name] = animation;
        }
    };
    SpriteRenderer.prototype._clearMesh = function () {
        if (this.threeMesh == null)
            return;
        this.actor.threeObject.remove(this.threeMesh);
        this.geometry.dispose();
        this.material.dispose();
        this.threeMesh = null;
    };
    SpriteRenderer.prototype._destroy = function () {
        this._clearMesh();
        this.asset = null;
        _super.prototype._destroy.call(this);
    };
    SpriteRenderer.prototype.setFrame = function (frame) {
        var framesPerRow = Math.floor(this.material.map.image.width / this.asset.grid.width);
        var frameX = frame % framesPerRow;
        var frameY = Math.floor(frame / framesPerRow);
        var left = (frameX * this.asset.grid.width) / this.material.map.image.width;
        var right = ((frameX + 1) * this.asset.grid.width) / this.material.map.image.width;
        var bottom = (this.material.map.image.height - (frameY + 1) * this.asset.grid.height) / this.material.map.image.height;
        var top = (this.material.map.image.height - frameY * this.asset.grid.height) / this.material.map.image.height;
        var uvs = this.geometry.getAttribute("uv");
        uvs.needsUpdate = true;
        uvs.array[0] = left;
        uvs.array[1] = top;
        uvs.array[2] = right;
        uvs.array[3] = top;
        uvs.array[4] = left;
        uvs.array[5] = bottom;
        uvs.array[6] = right;
        uvs.array[7] = bottom;
    };
    SpriteRenderer.prototype.setAnimation = function (newAnimationName, newAnimationLooping) {
        if (newAnimationLooping === void 0) { newAnimationLooping = true; }
        if (newAnimationName != null) {
            if (this.animationsByName[newAnimationName] == null)
                throw new Error("Animation " + newAnimationName + " doesn't exist");
            this.animationLooping = newAnimationLooping;
            if (newAnimationName === this.animationName && this.isAnimationPlaying)
                return;
            this.animationName = newAnimationName;
            this.animationTimer = 0;
            this.isAnimationPlaying = true;
            this.updateFrame();
        }
        else {
            this.animationName = null;
            this.setFrame(0);
        }
    };
    SpriteRenderer.prototype.getAnimation = function () { return this.animationName; };
    SpriteRenderer.prototype.setAnimationTime = function (time) {
        if (typeof time !== "number")
            throw new Error("Time must be an integer");
        if (time < 0 || time > this.getAnimationDuration())
            throw new Error("Time must be between 0 and " + this.getAnimationDuration());
        this.animationTimer = time * SupEngine.GameInstance.framesPerSecond;
        this.updateFrame();
    };
    SpriteRenderer.prototype.getAnimationTime = function () { return (this.animationName != null) ? this.animationTimer / SupEngine.GameInstance.framesPerSecond : 0; };
    SpriteRenderer.prototype.getAnimationDuration = function () {
        if (this.animationName != null) {
            var animation = this.animationsByName[this.animationName];
            return (animation.endFrameIndex - animation.startFrameIndex + 1) / this.asset.framesPerSecond;
        }
        return 0;
    };
    SpriteRenderer.prototype.playAnimation = function (animationLooping) {
        if (animationLooping === void 0) { animationLooping = true; }
        this.animationLooping = animationLooping;
        this.isAnimationPlaying = true;
    };
    SpriteRenderer.prototype.pauseAnimation = function () { this.isAnimationPlaying = false; };
    SpriteRenderer.prototype.stopAnimation = function () {
        if (this.animationName == null)
            return;
        this.isAnimationPlaying = false;
        this.animationTimer = 0;
        this.updateFrame();
    };
    SpriteRenderer.prototype.updateFrame = function () {
        this.hasFrameBeenUpdated = true;
        var animation = this.animationsByName[this.animationName];
        var frame = animation.startFrameIndex + Math.max(1, Math.ceil(this.animationTimer / SupEngine.GameInstance.framesPerSecond * this.asset.framesPerSecond)) - 1;
        if (frame > animation.endFrameIndex) {
            if (this.animationLooping) {
                frame = animation.startFrameIndex;
                this.animationTimer = 1;
            }
            else {
                frame = animation.endFrameIndex;
                this.isAnimationPlaying = false;
            }
        }
        this.setFrame(frame);
    };
    SpriteRenderer.prototype.update = function () {
        if (this.hasFrameBeenUpdated) {
            this.hasFrameBeenUpdated = false;
            return;
        }
        this._tickAnimation();
        this.hasFrameBeenUpdated = false;
    };
    SpriteRenderer.prototype._tickAnimation = function () {
        if (this.animationName == null || !this.isAnimationPlaying)
            return;
        this.animationTimer += 1;
        this.updateFrame();
    };
    SpriteRenderer.Updater = SpriteRendererUpdater_1.default;
    return SpriteRenderer;
})(SupEngine.ActorComponent);
exports.default = SpriteRenderer;

},{"./SpriteRendererUpdater":3}],3:[function(require,module,exports){
var THREE = SupEngine.THREE;
var SpriteRendererUpdater = (function () {
    function SpriteRendererUpdater(client, spriteRenderer, config, receiveAssetCallbacks, editAssetCallbacks) {
        this.spriteSubscriber = {
            onAssetReceived: this._onSpriteAssetReceived.bind(this),
            onAssetEdited: this._onSpriteAssetEdited.bind(this),
            onAssetTrashed: this._onSpriteAssetTrashed.bind(this)
        };
        this.client = client;
        this.spriteRenderer = spriteRenderer;
        this.receiveAssetCallbacks = receiveAssetCallbacks;
        this.editAssetCallbacks = editAssetCallbacks;
        this.spriteAssetId = config.spriteAssetId;
        this.animationId = config.animationId;
        this.spriteAsset = null;
        if (this.spriteAssetId != null)
            this.client.subAsset(this.spriteAssetId, "sprite", this.spriteSubscriber);
    }
    SpriteRendererUpdater.prototype.destroy = function () {
        if (this.spriteAssetId != null)
            this.client.unsubAsset(this.spriteAssetId, this.spriteSubscriber);
    };
    SpriteRendererUpdater.prototype._onSpriteAssetReceived = function (assetId, asset) {
        var _this = this;
        this.spriteAsset = asset;
        var image = (asset.pub.texture != null) ? asset.pub.texture.image : null;
        if (image == null) {
            image = new Image();
            asset.pub.texture = new THREE.Texture(image);
            if (asset.pub.filtering === "pixelated") {
                asset.pub.texture.magFilter = THREE.NearestFilter;
                asset.pub.texture.minFilter = THREE.NearestFilter;
            }
            if (this.url != null)
                URL.revokeObjectURL(this.url);
            var typedArray = new Uint8Array(asset.pub.image);
            var blob = new Blob([typedArray], { type: "image/*" });
            this.url = URL.createObjectURL(blob);
            image.src = this.url;
        }
        if (!image.complete) {
            if (asset.pub.image.byteLength === 0) {
                if (this.receiveAssetCallbacks != null)
                    this.receiveAssetCallbacks.sprite(null);
            }
            else {
                var onImageLoaded = function () {
                    image.removeEventListener("load", onImageLoaded);
                    asset.pub.texture.needsUpdate = true;
                    _this.spriteRenderer.setSprite(asset.pub);
                    if (_this.animationId != null)
                        _this._playAnimation();
                    if (_this.receiveAssetCallbacks != null)
                        _this.receiveAssetCallbacks.sprite(_this.url);
                };
                image.addEventListener("load", onImageLoaded);
            }
        }
        else {
            this.spriteRenderer.setSprite(asset.pub);
            if (this.animationId != null)
                this._playAnimation();
            if (this.receiveAssetCallbacks != null)
                this.receiveAssetCallbacks.sprite(this.url);
        }
    };
    SpriteRendererUpdater.prototype._playAnimation = function () {
        var animation = this.spriteAsset.animations.byId[this.animationId];
        if (animation == null)
            return;
        this.spriteRenderer.setAnimation(animation.name);
    };
    SpriteRendererUpdater.prototype._onSpriteAssetEdited = function (id, command) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var callEditCallback = true;
        var commandFunction = this[("_onEditCommand_" + command)];
        if (commandFunction != null) {
            if (commandFunction.apply(this, args) === false)
                callEditCallback = false;
        }
        if (callEditCallback && this.editAssetCallbacks != null) {
            var editCallback = this.editAssetCallbacks.sprite[command];
            if (editCallback != null)
                editCallback.apply(null, args);
        }
    };
    SpriteRendererUpdater.prototype._onEditCommand_upload = function () {
        var _this = this;
        if (this.url != null)
            URL.revokeObjectURL(this.url);
        var typedArray = new Uint8Array(this.spriteAsset.pub.image);
        var blob = new Blob([typedArray], { type: "image/*" });
        this.url = URL.createObjectURL(blob);
        var image = this.spriteAsset.pub.texture.image;
        image.src = this.url;
        image.addEventListener("load", function () { _this.spriteAsset.pub.texture.needsUpdate = true; });
        if (this.editAssetCallbacks != null)
            this.editAssetCallbacks.sprite.upload(this.url);
        return false;
    };
    SpriteRendererUpdater.prototype._onEditCommand_setProperty = function (path, value) {
        if (path === "filtering") {
            if (this.spriteAsset.pub.filtering === "pixelated") {
                this.spriteAsset.pub.texture.magFilter = THREE.NearestFilter;
                this.spriteAsset.pub.texture.minFilter = THREE.NearestFilter;
            }
            else {
                this.spriteAsset.pub.texture.magFilter = THREE.LinearFilter;
                this.spriteAsset.pub.texture.minFilter = THREE.LinearMipMapLinearFilter;
            }
            this.spriteAsset.pub.texture.needsUpdate = true;
        }
        else {
            this.spriteRenderer.setSprite(this.spriteAsset.pub);
            if (this.animationId != null)
                this._playAnimation();
        }
    };
    SpriteRendererUpdater.prototype._onEditCommand_newAnimation = function () {
        this.spriteRenderer.updateAnimationsByName();
        this._playAnimation();
    };
    SpriteRendererUpdater.prototype._onEditCommand_deleteAnimation = function () {
        this.spriteRenderer.updateAnimationsByName();
        this._playAnimation();
    };
    SpriteRendererUpdater.prototype._onEditCommand_setAnimationProperty = function () {
        this.spriteRenderer.updateAnimationsByName();
        this._playAnimation();
    };
    SpriteRendererUpdater.prototype._onSpriteAssetTrashed = function () {
        this.spriteRenderer.setSprite(null);
        // FIXME: the updater shouldn't be dealing with SupClient.onAssetTrashed directly
        if (this.editAssetCallbacks != null)
            SupClient.onAssetTrashed();
    };
    SpriteRendererUpdater.prototype.config_setProperty = function (path, value) {
        switch (path) {
            case "spriteAssetId": {
                if (this.spriteAssetId != null)
                    this.client.unsubAsset(this.spriteAssetId, this.spriteSubscriber);
                this.spriteAssetId = value;
                this.spriteAsset = null;
                this.spriteRenderer.setSprite(null);
                if (this.spriteAssetId != null)
                    this.client.subAsset(this.spriteAssetId, "sprite", this.spriteSubscriber);
                break;
            }
            case "animationId": {
                this.animationId = value;
                if (this.spriteAsset != null) {
                    if (this.animationId != null)
                        this._playAnimation();
                    else
                        this.spriteRenderer.setAnimation(null);
                }
                break;
            }
        }
    };
    return SpriteRendererUpdater;
})();
exports.default = SpriteRendererUpdater;

},{}]},{},[1]);
