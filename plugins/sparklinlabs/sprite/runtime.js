(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var SpriteRenderer = require("./SpriteRenderer");
var sprite = require("./sprite");
SupRuntime.registerPlugin("SpriteRenderer", SpriteRenderer);
SupRuntime.registerPlugin("sprite", sprite);

},{"./SpriteRenderer":2,"./sprite":3}],2:[function(require,module,exports){
function setupComponent(player, component, config) {
    if (config.spriteAssetId != null) {
        var sprite = player.getOuterAsset(config.spriteAssetId).__inner;
        component.setSprite(sprite);
        if (config.animationId != null) {
            // FIXME: should we load sprite with SupCore.data?
            sprite.animations.every(function (animation) {
                if (animation.id === config.animationId) {
                    component.setAnimation(animation.name);
                    return false;
                }
                else
                    return true;
            });
        }
    }
}
exports.setupComponent = setupComponent;

},{}],3:[function(require,module,exports){
function loadAsset(player, entry, callback) {
    player.getAssetData("assets/" + entry.id + "/asset.json", "json", function (err, data) {
        var img = new Image();
        img.onload = function () {
            data.texture = new SupEngine.THREE.Texture(img);
            data.texture.needsUpdate = true;
            if (data.filtering === "pixelated") {
                data.texture.magFilter = SupEngine.THREE.NearestFilter;
                data.texture.minFilter = SupEngine.THREE.NearestFilter;
            }
            callback(null, data);
        };
        img.onerror = function () { callback(null, data); };
        img.src = player.dataURL + "assets/" + entry.id + "/image.dat";
    });
}
exports.loadAsset = loadAsset;
function createOuterAsset(player, asset) { return new window.Sup.Sprite(asset); }
exports.createOuterAsset = createOuterAsset;

},{}]},{},[1]);
