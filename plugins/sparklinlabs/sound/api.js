(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

SupAPI.registerPlugin("typescript", "Sup.Sound", {
    code: "module Sup { export class Sound extends Asset {} }",
    defs: "declare module Sup { class Sound extends Asset { dummySoundMember; } }"
});
SupAPI.registerPlugin("typescript", "Sup.Audio", {
    code: "module Sup {\r\n  export module Audio {\r\n    export function getMasterVolume(volume) { return player.gameInstance.audio.masterGain.gain.value }\r\n    export function setMasterVolume(volume) { player.gameInstance.audio.masterGain.gain.value = volume }\r\n  }\r\n}",
    defs: "declare module Sup {\r\n  module Audio {\r\n    function getMasterVolume(): number;\r\n    function setMasterVolume(volume: number): void;\r\n  }\r\n}\r\n"
});
SupAPI.registerPlugin("typescript", "Sup.Audio.SoundInstance", {
    code: "module Sup {\r\n  export module Audio {\r\n    export class SoundInstance {\r\n      __inner: any;\r\n      constructor(asset) {\r\n        var audioCtx = player.gameInstance.audio.getContext();\r\n        var audioMasterGain = player.gameInstance.audio.masterGain;\r\n        this.__inner = new SupEngine.SoundInstance(audioCtx, audioMasterGain, asset.__inner.buffer);\r\n        return\r\n      }\r\n      play() { this.__inner.play(); return this }\r\n      stop() { this.__inner.stop(); return this }\r\n      pause() { this.__inner.pause(); return this }\r\n\r\n      getLoop() { return this.__inner.isLooping }\r\n      setLoop(looping) { this.__inner.setLoop(looping); return this }\r\n      getVolume() { return this.__inner.volume }\r\n      setVolume(volume) { this.__inner.setVolume(volume); return this }\r\n      getPan() { return this.__inner.pan }\r\n      setPan(pan) { this.__inner.setPan(pan); return this }\r\n      getPitch() { return this.__inner.pitch }\r\n      setPitch(pitch) { this.__inner.setPitch(pitch); return this }\r\n    }\r\n  }\r\n}\r\n",
    defs: "declare module Sup {\r\n  module Audio {\r\n    class SoundInstance {\r\n      constructor(asset: Sound);\r\n      play(): SoundInstance;\r\n      stop(): SoundInstance;\r\n      pause(): SoundInstance;\r\n\r\n      getLoop(): boolean;\r\n      setLoop(looping: boolean): SoundInstance;\r\n      getVolume(): number;\r\n      setVolume(volume: number): SoundInstance;\r\n      getPan(): number;\r\n      setPan(pan: number): SoundInstance;\r\n      getPitch(): number;\r\n      setPitch(pitch: number): SoundInstance;\r\n    }\r\n  }\r\n}\r\n"
});

},{}]},{},[1]);
