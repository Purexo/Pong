(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var epsilon = 0.01;
var THREE = SupEngine.THREE;
var ArcadeBody2D_1 = require("./ArcadeBody2D");
var ArcadeBody2DMarker_1 = require("./ArcadeBody2DMarker");
var ArcadePhysics2D;
(function (ArcadePhysics2D) {
    ArcadePhysics2D.allBodies = [];
    ArcadePhysics2D.gravity = new THREE.Vector3(0, 0, 0);
    function intersects(body1, body2) {
        if (body2.type === "tileMap")
            throw new Error("ArcadePhysics2D.intersects isn't supported for tileMap");
        if (body1.right() < body2.left())
            return false;
        if (body1.bottom() > body2.top())
            return false;
        if (body1.left() > body2.right())
            return false;
        if (body1.top() < body2.bottom())
            return false;
        return true;
    }
    ArcadePhysics2D.intersects = intersects;
    function collides(body1, bodies) {
        if (body1.type === "tileMap" || !body1.movable)
            throw new Error("The first body must be a movable box in ArcadePhysics2D.collides");
        body1.touches.top = false;
        body1.touches.bottom = false;
        body1.touches.right = false;
        body1.touches.left = false;
        var gotCollision = false;
        for (var _i = 0; _i < bodies.length; _i++) {
            var otherBody = bodies[_i];
            if (otherBody === body1)
                continue;
            if (otherBody.type === "box") {
                if (intersects(body1, otherBody) !== false) {
                    gotCollision = true;
                    var insideX = body1.position.x - otherBody.position.x;
                    if (insideX >= 0)
                        insideX -= (body1.width + otherBody.width) / 2;
                    else
                        insideX += (body1.width + otherBody.width) / 2;
                    var insideY = body1.position.y - otherBody.position.y;
                    if (insideY >= 0)
                        insideY -= (body1.height + otherBody.height) / 2;
                    else
                        insideY += (body1.height + otherBody.height) / 2;
                    if (Math.abs(insideY) <= Math.abs(insideX)) {
                        if (body1.deltaY() / insideY > 0) {
                            body1.velocity.y = -body1.velocity.y * body1.bounceY;
                            body1.position.y -= insideY;
                            if (body1.position.y > otherBody.position.y)
                                body1.touches.bottom = true;
                            else
                                body1.touches.top = true;
                        }
                    }
                    else {
                        if (body1.deltaX() / insideX > 0) {
                            body1.velocity.x = -body1.velocity.x * body1.bounceX;
                            body1.position.x -= insideX;
                            if (body1.position.x > otherBody.position.x)
                                body1.touches.left = true;
                            else
                                body1.touches.right = true;
                        }
                    }
                }
            }
            else if (otherBody.type === "tileMap") {
                function checkY(mapBody) {
                    if (body1.deltaY() < 0) {
                        var x = body1.position.x - mapBody.position.x - body1.width / 2;
                        var y = Math.floor((body1.position.y - mapBody.position.y - body1.height / 2) / mapBody.mapToSceneFactor);
                        var testedWidth = body1.width - epsilon;
                        var totalPoints = Math.ceil(testedWidth / mapBody.mapToSceneFactor) + 1;
                        for (var point = 0; point <= totalPoints; point++) {
                            for (var layer in mapBody.layersIndex) {
                                var tile = mapBody.tileMapAsset.getTileAt(layer, Math.floor((x + point * testedWidth / totalPoints) / mapBody.mapToSceneFactor), y);
                                var solidProperty = mapBody.tileSetAsset.getTileProperties(tile)[mapBody.tileSetPropertyName];
                                if (solidProperty != null) {
                                    gotCollision = true;
                                    body1.velocity.y = -body1.velocity.y * body1.bounceY;
                                    body1.position.y = (y + 1) * mapBody.mapToSceneFactor + mapBody.position.y + body1.height / 2;
                                    body1.touches.bottom = true;
                                    return;
                                }
                            }
                        }
                    }
                    else if (body1.deltaY() > 0) {
                        var x = body1.position.x - mapBody.position.x - body1.width / 2;
                        var y = Math.floor((body1.position.y - mapBody.position.y + body1.height / 2 - epsilon) / mapBody.mapToSceneFactor);
                        var testedWidth = body1.width - epsilon;
                        var totalPoints = Math.ceil(testedWidth / mapBody.mapToSceneFactor) + 1;
                        for (var point = 0; point <= totalPoints; point++) {
                            for (var layer in mapBody.layersIndex) {
                                var tile = mapBody.tileMapAsset.getTileAt(layer, Math.floor((x + point * testedWidth / totalPoints) / mapBody.mapToSceneFactor), y);
                                var solidProperty = mapBody.tileSetAsset.getTileProperties(tile)[mapBody.tileSetPropertyName];
                                if (solidProperty != null) {
                                    gotCollision = true;
                                    body1.velocity.y = -body1.velocity.y * body1.bounceY;
                                    body1.position.y = y * mapBody.mapToSceneFactor + mapBody.position.y - body1.height / 2;
                                    body1.touches.bottom = true;
                                    return;
                                }
                            }
                        }
                    }
                }
                function checkX(mapBody) {
                    if (body1.deltaX() < 0) {
                        var x = Math.floor((body1.position.x - mapBody.position.x - body1.width / 2) / mapBody.mapToSceneFactor);
                        var y = body1.position.y - mapBody.position.y - body1.height / 2;
                        var testedHeight = body1.height - epsilon;
                        var totalPoints = Math.ceil(testedHeight / mapBody.mapToSceneFactor) + 1;
                        for (var point = 0; point <= totalPoints; point++) {
                            for (var layer in mapBody.layersIndex) {
                                var tile = mapBody.tileMapAsset.getTileAt(layer, x, Math.floor((y + point * testedHeight / totalPoints) / mapBody.mapToSceneFactor));
                                var solidProperty = mapBody.tileSetAsset.getTileProperties(tile)[mapBody.tileSetPropertyName];
                                if (solidProperty != null) {
                                    gotCollision = true;
                                    body1.velocity.x = -body1.velocity.x * body1.bounceX;
                                    body1.position.x = (x + 1) * mapBody.mapToSceneFactor + mapBody.position.x + body1.width / 2;
                                    body1.touches.left = true;
                                    return true;
                                }
                            }
                        }
                    }
                    else if (body1.deltaX() > 0) {
                        var x = Math.floor((body1.position.x - mapBody.position.x + body1.width / 2 - epsilon) / mapBody.mapToSceneFactor);
                        var y = body1.position.y - mapBody.position.y - body1.height / 2;
                        var testedHeight = body1.height - epsilon;
                        var totalPoints = Math.ceil(testedHeight / mapBody.mapToSceneFactor) + 1;
                        for (var point = 0; point <= totalPoints; point++) {
                            for (var layer in mapBody.layersIndex) {
                                var tile = mapBody.tileMapAsset.getTileAt(layer, x, Math.floor((y + point * testedHeight / totalPoints) / mapBody.mapToSceneFactor));
                                var solidProperty = mapBody.tileSetAsset.getTileProperties(tile)[mapBody.tileSetPropertyName];
                                if (solidProperty != null) {
                                    gotCollision = true;
                                    body1.velocity.x = -body1.velocity.x * body1.bounceX;
                                    body1.position.x = x * mapBody.mapToSceneFactor + mapBody.position.x - body1.width / 2;
                                    body1.touches.left = true;
                                    return true;
                                }
                            }
                        }
                    }
                    return false;
                }
                var yPosition = body1.position.y;
                var ySpeed = body1.velocity.y;
                checkY(otherBody);
                if (checkX(otherBody)) {
                    body1.position.y = yPosition;
                    body1.velocity.y = ySpeed;
                    checkY(otherBody);
                }
            }
        }
        if (gotCollision)
            body1.refreshActorPosition();
        return gotCollision;
    }
    ArcadePhysics2D.collides = collides;
})(ArcadePhysics2D || (ArcadePhysics2D = {}));
SupEngine.ArcadePhysics2D = ArcadePhysics2D;
SupEngine.registerEarlyUpdateFunction("ArcadePhysics2D", function () {
    for (var _i = 0, _a = ArcadePhysics2D.allBodies; _i < _a.length; _i++) {
        var body = _a[_i];
        body.earlyUpdate();
    }
});
SupEngine.registerComponentClass("ArcadeBody2D", ArcadeBody2D_1.default);
SupEngine.registerEditorComponentClass("ArcadeBody2DMarker", ArcadeBody2DMarker_1.default);

},{"./ArcadeBody2D":2,"./ArcadeBody2DMarker":3}],2:[function(require,module,exports){
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var THREE = SupEngine.THREE;
var ArcadeBody2D = (function (_super) {
    __extends(ArcadeBody2D, _super);
    function ArcadeBody2D(actor, type, config) {
        _super.call(this, actor, "ArcadeBody2D");
        this.movable = false;
        this.width = 1;
        this.height = 1;
        this.offsetX = 0;
        this.offsetY = 0;
        this.bounceX = 0;
        this.bounceY = 0;
        this.layersIndex = [];
        this.touches = { top: false, bottom: false, right: false, left: false };
        if (type === "box")
            this.setupBox(config);
        else if (type === "tileMap")
            this.setupTileMap(config);
        SupEngine.ArcadePhysics2D.allBodies.push(this);
    }
    ArcadeBody2D.prototype.setupBox = function (config) {
        this.type = "box";
        this.movable = config.movable;
        this.width = config.width;
        this.height = config.height;
        if (config.offsetX != null)
            this.offsetX = config.offsetX;
        if (config.offsetY != null)
            this.offsetY = config.offsetY;
        if (config.bounceX != null)
            this.bounceX = config.bounceX;
        if (config.bounceY != null)
            this.bounceY = config.bounceY;
        this.actorPosition = this.actor.getGlobalPosition();
        this.position = this.actorPosition.clone();
        this.position.x += this.offsetX;
        this.position.y += this.offsetY;
        this.previousPosition = this.position.clone();
        this.velocity = new THREE.Vector3(0, 0, 0);
        this.velocityMin = new THREE.Vector3(-Infinity, -Infinity, 0);
        this.velocityMax = new THREE.Vector3(Infinity, Infinity, 0);
        this.velocityMultiplier = new THREE.Vector3(1, 1, 0);
    };
    ArcadeBody2D.prototype.setupTileMap = function (config) {
        this.type = "tileMap";
        this.tileMapAsset = config.tileMapAsset;
        this.tileSetAsset = config.tileSetAsset;
        this.mapToSceneFactor = this.tileSetAsset.__inner.data.gridSize / this.tileMapAsset.__inner.data.pixelsPerUnit;
        this.tileSetPropertyName = config.tileSetPropertyName;
        if (config.layersIndex != null) {
            var layers = config.layersIndex.split(",");
            for (var layer in layers)
                this.layersIndex.push(parseInt(layer));
        }
        else {
            for (var i = 0; i < this.tileMapAsset.__inner.data.layers.length; i++)
                this.layersIndex.push(i);
        }
        this.position = this.actor.getGlobalPosition();
    };
    ArcadeBody2D.prototype.earlyUpdate = function () {
        if (!this.movable)
            return;
        this.previousPosition.copy(this.position);
        this.velocity.x += SupEngine.ArcadePhysics2D.gravity.x;
        this.velocity.x *= this.velocityMultiplier.x;
        this.velocity.y += SupEngine.ArcadePhysics2D.gravity.y;
        this.velocity.y *= this.velocityMultiplier.y;
        if (this.velocity.length() !== 0) {
            this.velocity.x = Math.min(Math.max(this.velocity.x, this.velocityMin.x), this.velocityMax.x);
            this.velocity.y = Math.min(Math.max(this.velocity.y, this.velocityMin.y), this.velocityMax.y);
            this.position.add(this.velocity);
            this.refreshActorPosition();
        }
    };
    ArcadeBody2D.prototype.warpPosition = function (position) {
        this.position.x = position.x + this.offsetX;
        this.position.y = position.y + this.offsetY;
        this.refreshActorPosition();
    };
    ArcadeBody2D.prototype.refreshActorPosition = function () {
        this.actorPosition.x = this.position.x - this.offsetX;
        this.actorPosition.y = this.position.y - this.offsetY;
        this.actor.setGlobalPosition(this.actorPosition.clone());
    };
    ArcadeBody2D.prototype._destroy = function () {
        SupEngine.ArcadePhysics2D.allBodies.splice(SupEngine.ArcadePhysics2D.allBodies.indexOf(this), 1);
        _super.prototype._destroy.call(this);
    };
    ArcadeBody2D.prototype.right = function () { return this.position.x + this.width / 2; };
    ArcadeBody2D.prototype.left = function () { return this.position.x - this.width / 2; };
    ArcadeBody2D.prototype.top = function () { return this.position.y + this.height / 2; };
    ArcadeBody2D.prototype.bottom = function () { return this.position.y - this.height / 2; };
    ArcadeBody2D.prototype.deltaX = function () { return this.position.x - this.previousPosition.x; };
    ArcadeBody2D.prototype.deltaY = function () { return this.position.y - this.previousPosition.y; };
    return ArcadeBody2D;
})(SupEngine.ActorComponent);
exports.default = ArcadeBody2D;

},{}],3:[function(require,module,exports){
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var THREE = SupEngine.THREE;
var ArcadeBody2DUpdater_1 = require("./ArcadeBody2DUpdater");
var ArcadeBody2DMarker = (function (_super) {
    __extends(ArcadeBody2DMarker, _super);
    function ArcadeBody2DMarker(actor) {
        _super.call(this, actor, "ArcadeBody2DMarker");
        this.offset = new THREE.Vector3(0, 0, 0);
    }
    ArcadeBody2DMarker.prototype.setBox = function (width, height) {
        if (this.line != null)
            this._clearRenderer();
        var geometry = new THREE.Geometry();
        geometry.vertices.push(new THREE.Vector3(-width / 2, -height / 2, 0.01));
        geometry.vertices.push(new THREE.Vector3(width / 2, -height / 2, 0.01));
        geometry.vertices.push(new THREE.Vector3(width / 2, height / 2, 0.01));
        geometry.vertices.push(new THREE.Vector3(-width / 2, height / 2, 0.01));
        geometry.vertices.push(new THREE.Vector3(-width / 2, -height / 2, 0.01));
        var material = new THREE.LineBasicMaterial({ color: 0xf459e4 });
        this.line = new THREE.Line(geometry, material);
        this.line.position.copy(this.offset);
        this.actor.threeObject.add(this.line);
        this.line.updateMatrixWorld(false);
    };
    ArcadeBody2DMarker.prototype.setOffset = function (x, y) {
        this.offset.set(x, y, 0);
        this.line.position.copy(this.offset);
        this.line.updateMatrixWorld(false);
    };
    ArcadeBody2DMarker.prototype.setTileMap = function () {
        if (this.line != null)
            this._clearRenderer();
        // TODO ?
    };
    ArcadeBody2DMarker.prototype._clearRenderer = function () {
        this.actor.threeObject.remove(this.line);
        this.line.geometry.dispose();
        this.line.material.dispose();
        this.line = null;
    };
    ArcadeBody2DMarker.prototype._destroy = function () {
        if (this.line != null)
            this._clearRenderer();
        _super.prototype._destroy.call(this);
    };
    ArcadeBody2DMarker.Updater = ArcadeBody2DUpdater_1.default;
    return ArcadeBody2DMarker;
})(SupEngine.ActorComponent);
exports.default = ArcadeBody2DMarker;

},{"./ArcadeBody2DUpdater":4}],4:[function(require,module,exports){
var ArcadeBody2DUpdater = (function () {
    function ArcadeBody2DUpdater(client, bodyRenderer, config) {
        this.bodyRenderer = bodyRenderer;
        this.config = config;
        this.setType();
    }
    ArcadeBody2DUpdater.prototype.destroy = function () { };
    ArcadeBody2DUpdater.prototype.config_setProperty = function (path, value) {
        this.config[path] = value;
        if (path === "width" || path === "height")
            this.bodyRenderer.setBox(this.config.width, this.config.height);
        if (path === "offsetX" || path === "offsetY")
            this.bodyRenderer.setOffset(this.config.offsetX, this.config.offsetY);
        if (path === "type")
            this.setType();
    };
    ArcadeBody2DUpdater.prototype.setType = function () {
        if (this.config.type === "box") {
            this.bodyRenderer.setBox(this.config.width, this.config.height);
            this.bodyRenderer.setOffset(this.config.offsetX, this.config.offsetY);
        }
        else
            this.bodyRenderer.setTileMap();
    };
    return ArcadeBody2DUpdater;
})();
exports.default = ArcadeBody2DUpdater;

},{}]},{},[1]);
